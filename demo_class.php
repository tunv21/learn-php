<?php
    class Demo{
        private $name;
        var $name2;
        function getName(){
            return $this->name;
        }
        function setName($name, $nam2){
            $this->name = $name;
            $this->name2 = $nam2;
        }
        function toString(){
            return $this->name. '-' . $this->name2;
        }
    }

    class Test extends Demo{
        function out(){
            $this->setName('nguyen', 'van');
            echo $this->name2. '<br/>';
            echo $this->toString();
        }
    }

    $test = new Test();
    $test->out();
    // $demo = new Demo();
    // $demo->setName('nguyen', 'van');
    // echo $demo->toString();
?>